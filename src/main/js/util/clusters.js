const clusters = [
  {
    key: 'ADM',
    text: 'ADM',
    value: 'ADM'
  },
  {
    key: 'DOCUMENTATION',
    text: 'DOCUMENTATION',
    value: 'DOCUMENTATION'
  },
  {
    key: 'EASC',
    text: 'EASC',
    value: 'EASC'
  },
  {
    key: 'GEDI',
    text: 'GEDI',
    value: 'GEDI'
  },
  {
    key: 'MGT',
    text: 'MGT',
    value: 'MGT'
  },
  {
    key: 'MIS',
    text: 'MIS',
    value: 'MIS'
  },
  {
    key: 'NETOPS',
    text: 'NETOPS',
    value: 'NETOPS'
  },
  {
    key: 'OPERATIONS',
    text: 'OPERATIONS',
    value: 'OPERATIONS'
  },
  {
    key: 'PLANNING',
    text: 'PLANNING',
    value: 'PLANNING'
  },
  {
    key: 'SHIPMENT SERVICES',
    text: 'SHIPMENT SERVICES',
    value: 'SHIPMENT SERVICES'
  },
  {
    key: 'SYSADM',
    text: 'SYSADM',
    value: 'SYSADM'
  },
  {
    key: 'VDC',
    text: 'VDC',
    value: 'VDC'
  },
  {
    key: 'RELEASE MANAGEMENT',
    text: 'RELEASE MANAGEMENT',
    value: 'RELEASE MANAGEMENT'
  }
]

export default clusters