const attributes = [
    {
        label: 'Positive',
        code: 'positive',
        value: false
    },
    {
        label: 'Follow-Up',
        code: 'followUp',
        value: false
    },
    {
        label: 'Creativity',
        code: 'creativity',
        value: false
    },
    {
        label: 'Respect others Point of View',
        code: 'ropv',
        value: false
    },
    {
        label: 'Honesty',
        code: 'honesty',
        value: false
    },
    {
        label: 'Dedication',
        code: 'dedication',
        value: false
    },
    {
        label: 'Thoroughness',
        code: 'thoroughness',
        value: false
    },
    {
        label: 'Empower & Develop others',
        code: 'edo',
        value: false
    },
    {
        label: 'Sharing',
        code: 'sharing',
        value: false
    },
    {
        label: 'Accuracy',
        code: 'accuracy',
        value: false
    },
    {
        label: 'Skill Application',
        code: 'skillApplication',
        value: false
    },
    {
        label: 'Shares Info & Encourage Discussion',
        code: 'sied',
        value: false
    },
    {
        label: 'Flexible',
        code: 'flexible',
        value: false
    },
    {
        label: 'Timeliness',
        code: 'timeliness',
        value: false
    },
    {
        label: 'Extra Effort',
        code: 'extraEffort',
        value: false
    },
    {
        label: 'Recognizes achievements & gives credit',
        code: 'ragc',
        value: false
    },
    {
        label: 'Caring',
        code: 'caring',
        value: false
    },
    {
        label: 'Sensitivity',
        code: 'sensitivity',
        value: false
    },
    {
        label: 'Can Do Attitude',
        code: 'cda',
        value: false
    },
    {
        label: 'Customer Appreciation Letter',
        code: 'cal',
        value: false
    },
    {
        label: 'Quality',
        code: 'quality',
        value: false
    },

    {
        label: 'Responsiveness',
        code: 'responsiveness',
        value: false
    },
    {
        label: 'Is a Team Member',
        code: 'isATeamMember',
        value: false
    }

]


export default attributes
