import React, {Component} from 'react'
import {Menu} from "semantic-ui-react"
import {connect} from "react-redux"

export class NavBar extends Component {

    constructor(props){
        super(props)

    }

    handleItemClick (e, {name}) {
        console.log(this.props, name)
        if(this.props.onItemClick){
            this.props.onItemClick(name)
        }
    }

    handleLogInAndOutClick(){
        window.location.href = this.props.userProfile ? '/logout' : '/login'
    }

    render() {
        const menuItemName = this.props.userProfile ? 'logout' : 'login'

        return (
            <div >
                <Menu style={{height:60, backgroundColor:'#EB5757', color:'white'}}>
                    <Menu.Item style={{fontSize:20, color:'white'}} name='Recognition System'  onClick={this.handleItemClick} width={4}/>
                    <div style={{
                        display: 'flex',
                        paddingLeft: 20,
                        alignItems: 'center',
                        textTransform: 'capitalize',
                        fontSize: 36,
                        fontWeight: 500,
                        lineHeight: 42,
                        fontFamily: 'Roboto'
                    }}> {this.props.activeItem} </div>
                    <Menu.Menu position='right'>
                        <Menu.Item
                            style={{fontSize: 20, color: 'white'}}
                            name={menuItemName}
                            onClick={this.handleLogInAndOutClick.bind(this)}
                        />
                    </Menu.Menu>
                </Menu>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        userProfile: state.app.profile
    }
}
export default connect(mapStateToProps, null)(NavBar)