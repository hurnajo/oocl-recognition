import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Icon, Menu} from 'semantic-ui-react';

export class SideBar extends Component {

    constructor(props){
        super(props)
        this.state = { activeItem: 'bio' }
    }

    handleItemClick = (e, {name}) => {
        this.setState({ activeItem: name })
        if(this.props.onItemClick){
            this.props.onItemClick(name)
        }
    }

    checkIfApprover(authorities) {
        return authorities.some(auth => auth.authority === 'APPROVER')
    }

    render() {
        const { activeItem } = this.state
        const iconStyle = {float:'left', marginRight:15}
        const isApprover = (this.props.userProfile && this.props.userProfile.authorities.length) && this.checkIfApprover(this.props.userProfile.authorities)
        return (
            <Menu id='recog-sidebar' fluid vertical tabular style={{borderRight:'0px', paddingLeft:'inherit', backgroundColor:'#F2F2F2'}}>
                {/*<Menu.Item name='home' active={activeItem === 'home'} onClick={this.handleItemClick}>*/}
                    {/*<Icon name='home' style={iconStyle} /> Home*/}
                {/*</Menu.Item>*/}
                <Menu.Item name='achievement' active={activeItem === 'achievement'} onClick={this.handleItemClick}>
                    <Icon name='star' style={iconStyle} /> Achievement
                </Menu.Item>
                <Menu.Item name='request' active={activeItem === 'request'} onClick={this.handleItemClick}>
                    <Icon name='clipboard' style={iconStyle} /> Request
                </Menu.Item>
                {isApprover &&
                    <Menu.Item name='pendingApproval' active={activeItem === 'pendingApproval'} onClick={this.handleItemClick}>
                        <Icon name='check' style={iconStyle} /> Approvals
                    </Menu.Item>
                }
                {/*<Menu.Item name='maintenance' active={activeItem === 'maintenance'} onClick={this.handleItemClick}>*/}
                    {/*<Icon name='setting' style={iconStyle} /> Maintenance*/}
                {/*</Menu.Item>*/}
            </Menu>
        )
    }
}
function mapStateToProps(state) {
    return {
        userProfile: state.app.profile
    }
}
export default connect(mapStateToProps,null)(SideBar)