import React from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

import {Button, Icon, Modal} from "semantic-ui-react"
import {clearError} from "../../state/app/actions"

const modalStyle = {
    width: 500,
    height: 'auto',
    padding: 5,
    top: 'calc(50vh - 100px)',
    left: 'calc(50vw - 250px)'
}

export class ErrorModal extends React.Component {
    handleOkayClick() {
        this.props.actions.clearError()
        window.location.reload()
    }

    render() {
        return (
            <Modal size={"tiny"} open={this.props.hasError} style={modalStyle}>
                <Modal.Content>
                    <Icon name={'warning sign'}/>
                    Application error encountered! Please refresh app...
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        positive
                        size={'small'}
                        icon={'checkmark'}
                        labelPosition={'right'}
                        content={'Okay'}
                        onClick={this.handleOkayClick.bind(this)}
                    />
                </Modal.Actions>
            </Modal>
        )
    }
}

function mapStateToProps(state) {
    return {
      hasError: state.app.hasError,
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({clearError}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModal)