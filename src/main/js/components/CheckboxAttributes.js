import React, {Component} from 'react'
import {Checkbox, GridColumn} from "semantic-ui-react"

class CheckboxAttributes extends Component {
    render() {
        let {checked, width , name, label, checkboxChanged } = this.props
        return (
            <GridColumn  width={width}>
                <Checkbox name={name} label={label} onChange={checkboxChanged()} checked={checked}/>
            </GridColumn>
        )
    }
}

export default CheckboxAttributes