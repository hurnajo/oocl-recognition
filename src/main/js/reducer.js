import {combineReducers} from 'redux'
import request from './state/request/reducers'
import pendingApproval from './state/pendingApproval/reducers'
import app from './state/app/reducers'
import login from './state/login/reducers'
import maintenance from './state/maintenance/reducers'
import achievement from './state/achievement/reducers'
import users from './state/user/reducers'
import milestones from './state/milestone/reducers'

const rootReducer = combineReducers({
    request,
    pendingApproval,
    app,
    login,
    maintenance,
    achievement,
    users,
    milestones
})

export default rootReducer
