import * as types from '../../actionTypes'

const initialState = {
    user: []
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_USER: {
            return {
                ...state,
                user: action.payload
            }
        }
        case types.FETCH_ALL_USER_DROPDOWN: {
            return {
                ...state,
                usersDropDown: action.payload
            }
        }
        default:
            return state
    }
}