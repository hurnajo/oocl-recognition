import axios from 'axios'
import {deriveDateToday} from "../../util/DateUtil"
import UrlUtil from "../../util/UrlUtil";

const url = new UrlUtil('/api/awards')

export const submitRequest = (requestEntity) => {
    return async () => {
        requestEntity.requestedDate = deriveDateToday(requestEntity.requestDate)
        await axios.post(url.makeUrl(['saveAward']), JSON.stringify(requestEntity))
    }
}