import axios from 'axios'
import * as types from '../../actionTypes'
import UrlUtil from "../../util/UrlUtil"

const url = new UrlUtil('/api/milestone')

export const fetchAllMilestones = () => {
  return async (dispatch) => {
    dispatch({type: types.SHOW_LOADING})
    const response = await axios.get(url.makeUrl(['getAll']))

    let voyage = []

    response.data.forEach((data) => {
      data.voyage > 0 && voyage.push( {
          name: data.user.fullName,
          team: data.user.team,
          description: data.voyage + " Voyage",
          imageSrc: '/images/avatar/' + data.user.avatar
        })
    })

    let table = response.data.map((data, id) => {
      return {
        id: id,
        imageSrc: '/images/avatar/' + data.user.avatar,
        name: data.user.fullName,
        team: data.user.team,
        stars: data.points
      }
    })

    table.sort((a, b) => a.name.localeCompare(b.name))
    dispatch({type: types.FETCH_ALL_MILESTONES, payload: {voyage: voyage, table: table}})
    dispatch({type: types.HIDE_LOADING})
  }
}