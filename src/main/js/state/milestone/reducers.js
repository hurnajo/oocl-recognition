import * as types from '../../actionTypes'

const initialState = {
    voyage: [],
    table: []
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_ALL_MILESTONES: {
            return {
                ...state,
                voyage: action.payload.voyage,
                table : action.payload.table
            }
        }
        default:
            return state
    }
}