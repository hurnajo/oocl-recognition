import axios from 'axios'
import * as types from '../../actionTypes'


const url = '/api/login'

export const login = (username, password) => {
    return async (dispatch) => {
        const response = await axios.post(url,{username : username, password: password})
        dispatch({type: types.SAVE_USER, payload: response.data})
    }
}