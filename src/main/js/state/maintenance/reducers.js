import * as types from '../../actionTypes'

const initialState = {
    users: [],
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_USER: {
            return {
                ...state,
                users: action.payload
            }
        }
        default:
            return state
    }
}