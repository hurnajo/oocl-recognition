import * as types from '../../actionTypes'

const initialState = {
    appInitialized: false,
    profile: null,
    loading: false,
    loggedIn: false,
    hasError: false
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case types.FETCH_USER_PROFILE_REQUEST: {
            return {
                ...state,
                profile: null
            }
        }
        case types.FETCH_USER_PROFILE_SUCCESS: {
            return {
                ...state,
                profile: action.payload
            }
        }
        case types.HAS_ERROR: {
            return {
               ...state,
                hasError: true
            }
        }
        case types.CLEAR_ERROR: {
            return {
                ...state,
                hasError: false
            }
        }
        case types.APP_INITIALIZED: {
            return {
                ...state,
                appInitialized: true
            }
        }
        case types.SHOW_LOADING : {
            return {
                ...state,
                loading: true
            }
        }
        case types.HIDE_LOADING : {
            return {
                ...state,
                loading: false
            }
        }
        case types.LOGGED_IN_SUCCESS : {
            return {
                ...state,
                loggedIn: true
            }
        }
        default:
            return state
    }
}