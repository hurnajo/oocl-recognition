import React, {Component} from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

import {getProfile, initializeApp} from './state/app/actions'
import RequestPage from "./pages/RequestPage"
import MaintenancePage from "./pages/MaintenancePage"
import PendingApprovalPage from "./pages/PendingApprovalPage"
import {Route, Switch, withRouter} from "react-router-dom"
import AchievementPage from "./pages/AchievementPage"
import NavBar from "./components/NavBar"
import {Grid, Segment} from "semantic-ui-react"
import SideBar from "./components/SideBar"
import Loading from "./components/Loading"
import SockJsClient from 'react-stomp'
import HomePage from "./pages/HomePage"

import axios from "axios"
import {fetchAllMilestones} from "./state/milestone/actions"
import {fetchAllPendingApproval, fetchApprovedAward} from "./state/pendingApproval/actions"
import * as statics from "./util/statics"
import {RequestInfoModal} from "./components/modals/RequestInfoModal";
import ErrorModal from "./components/modals/ErrorModal"

axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8'


export class App extends Component {

    constructor(props){
        super(props)
        this.state = {
            clientConnected: false,
            messages: [],
            activeItem: 'Request',
            displayRequestInfoModal: false,
            approvedAward: null
        }
    }

    componentDidMount() {
        this.props.actions.getProfile()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.deriveActiveItem()
    }

    deriveActiveItem() {
        const uriPath = window.location.pathname.slice(1, window.location.pathname.length)
        let activeItem = ''
        switch(uriPath) {
            case 'achievement': {
                activeItem = 'Achievement'
                break
            }
            case 'request': {
                activeItem = 'Request'
                break
            }
            case 'pendingApproval': {
                activeItem = 'Approval'
                break
            }
            default: {
                activeItem = 'Request'
                break
            }
        }

        if(this.state.activeItem !== activeItem) {
            this.setState({activeItem: activeItem})
        }
    }


    notification (notification) {
        const isApprover = (this.props.userProfile && this.props.userProfile.authorities.length) && this.checkIfApprover(this.props.userProfile.authorities)
        let notif

        if(!("Notification" in window)) {
            alert("This browser does not support desktop notification")
        }else if(Notification.permission === "granted") {
            if(statics.REQUEST_PENDING === notification.action && isApprover) {
                notif = this.createNotification(this, 'New Request!', notification)
                notif.onclick = () => {this.props.history.push('/pendingApproval')}
            }else if([statics.MILESTONE_ACHIEVED, statics.REQUEST_APPROVED].includes(notification.action)){
                notif = this.createNotification(this, 'Congratulation!', notification)
                notif.onclick = () => this.displayRequestInfoCard(notification.awardId, notification.approver)
            }
            document.getElementById('notification-sound').play()
        }else if(Notification.permission !== "denied") {
            const me = this
            Notification.requestPermission().then(function (permission) {
                if (permission === "granted") {
                    if(statics.REQUEST_PENDING === notification.action && isApprover) {
                        notif = me.createNotification(this, 'New Request!', notification)
                        notif.onclick = () => {this.props.history.push('/pendingApproval')}
                    }else if([statics.MILESTONE_ACHIEVED, statics.REQUEST_APPROVED].includes(notification.action)){
                        notif = me.createNotification(this, 'Congratulation!', notification)
                        notif.onclick = () => me.displayRequestInfoCard(notification.awardId, notification.approver)
                    }
                    document.getElementById('notification-sound').play()
                }
            })
        }
    }

    createNotification(me, title, notification) {
        return new Notification(title, {
            body: notification.message,
            icon: `/images/avatar/${notification.image}`,
            // sound: '/sounds/facebook_sound.mp3', NOT SUPPORTED IN ANY BROWSER
            silent: false
        })
    }

    displayRequestInfoCard(awardId, approver) {
        this.props.actions.fetchApprovedAward(awardId, approver).then((response) => {
            console.log(response)
            this.setState({displayRequestInfoModal: true, approvedAward: response})
        })
    }

    sideBarMenuClicked = (activeItem) => {
        this.props.history.push('/' + activeItem)
        this.setState({activeItem : activeItem})
    }

    navBarMenuClicked = (activeItem) => {
        this.props.history.push('/' + activeItem)
    }

    sendMessage = (msg, selfMsg) => {
        try {
            this.clientRef.sendMessage("/app/all", JSON.stringify(selfMsg))
            return true
        } catch(e) {
            return false
        }
    }

    onMessageReceive = (notification, topic) => {
        if(notification.action === statics.REQUEST_APPROVED) {
            this.props.actions.fetchAllMilestones()
        }
        this.props.actions.fetchAllPendingApproval()

        this.setState(prevState => ({
            messages: [...prevState.messages, notification.message]
        }))

        this.notification(notification)
    }

    handleRequestInfoModalClose() {
        this.setState({displayRequestInfoModal: false})
    }

    checkIfApprover(authorities) {
        return authorities.some(auth => auth.authority === 'APPROVER')
    }

    render() {
        let wsSourceUrl = window.location.protocol + '//localhost:9090/handler'
        if(window.location.host.indexOf('localhost:8080') === -1) {
            wsSourceUrl = window.location.protocol + `//${window.location.host}/handler`
        }
        const isApprover = (this.props.userProfile && this.props.userProfile.authorities.length) && this.checkIfApprover(this.props.userProfile.authorities)

        return (
            <div>
                <audio
                    id={'notification-sound'}
                    preload={'auto'}
                >
                    <source src={'/sounds/facebook_sound.mp3'} type={'audio/mpeg'}/>
                    <embed hidden={true} autostart={'false'} loop={false} src='../resources/static/sounds/facebook_sound.mp3' />
                </audio>
                <ErrorModal/>
                <RequestInfoModal
                    open={this.state.displayRequestInfoModal}
                    onCloseFn={this.handleRequestInfoModalClose.bind(this)}
                    data={this.state.approvedAward}
                />
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                           <NavBar activeItem={this.state.activeItem} onItemClick={(activeItem) => this.navBarMenuClicked(activeItem)}/>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={2} style={{paddingRight:0}}>
                            <SideBar onItemClick={(activeItem) => this.sideBarMenuClicked(activeItem)}/>
                        </Grid.Column>
                        <Grid.Column stretched width={14}>
                            <Segment>
                                <Loading active={this.props.loading}/>
                                <Switch>
                                    <Route exact path="/" component={RequestPage}/>
                                    <Route exact path="/home" component={HomePage}/>
                                    <Route exact path="/maintenance" component={MaintenancePage}/>
                                    <Route exact path="/achievement" component={AchievementPage}/>
                                    {isApprover &&
                                        <Route exact path="/pendingApproval" component={PendingApprovalPage}/>
                                    }
                                    <Route exact path="/request" component={RequestPage}/>
                                </Switch>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    {<SockJsClient url={ wsSourceUrl } topics={["/topic/all"]}
                                   onMessage={ this.onMessageReceive } ref={ (client) => { this.clientRef = client }}
                                   onConnect={ () => { this.setState({ clientConnected: true }) } }
                                   onDisconnect={ () => { this.setState({ clientConnected: false }) } }
                                   heartbeat={3000}
                                   debug={ true }/>
                    }
                </Grid>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        appInitialized: state.app.appInitialized,
        loading : state.app.loading,
        loggedIn : state.app.loggedIn,
        userProfile: state.app.profile
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({initializeApp, fetchAllPendingApproval, fetchAllMilestones, fetchApprovedAward, getProfile}, dispatch)}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))