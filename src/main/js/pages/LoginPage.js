import React, {Component} from 'react'
import {login} from "../state/login/actions"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import {Button, Form, Grid, Input} from "semantic-ui-react"
import NavBar from "../components/NavBar"

export class LoginPage extends Component {
    render() {
        return (
            <div style={{backgroundColor: '#E5E5E5'}}>
                <Form>
                    <Form.Field>
                        <Input icon='user' iconPosition='left' placeholder='Email Address'/>
                    </Form.Field>
                    <Form.Field>
                        <Input icon='lock' iconPosition='left' placeholder='Password'/>
                    </Form.Field>

                    <Button type='login' style={{
                        width: '100%',
                        backgroundColor: '#EB5757',
                        color: 'white',
                        height: '57px'
                    }}>Login</Button>
                </Form>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userProfile: state.login.userProfile
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({login}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)