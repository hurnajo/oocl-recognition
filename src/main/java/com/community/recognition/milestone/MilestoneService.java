package com.community.recognition.milestone;

import com.community.recognition.award.AwardEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MilestoneService {

    private MilestoneRepository milestoneRepository;

    public Iterable<MilestoneEntity> getAll() {
        return milestoneRepository.findAll();
    }

    public MilestoneEntity findMilestoneByUserId(Long userId){
        return milestoneRepository.findByUserId(userId);
    }

    public MilestoneEntity save(MilestoneEntity milestoneEntity) {
        return milestoneRepository.save(milestoneEntity);
    }

    public boolean updateMilestone(AwardEntity awardEntity, int pointsToAdd) {
        boolean milestoneReached = false;
        MilestoneEntity milestoneEntity = findMilestoneByUserId(awardEntity.getAwardeeUserId());
        milestoneEntity.addPoints(pointsToAdd);
        if(milestoneEntity.isCompletedVoyage()) {
            milestoneEntity.increaseVoyageByOne();
            milestoneReached = true;
        }

        save(milestoneEntity);

        return milestoneReached;
    }

}
