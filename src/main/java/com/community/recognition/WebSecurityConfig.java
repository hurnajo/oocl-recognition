package com.community.recognition;

import com.community.recognition.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Lazy
    @Autowired
    private UserService userService;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userService);
        authProvider.setPasswordEncoder(new BCryptPasswordEncoder(11));
        auth.authenticationProvider(authProvider);
    }

    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/images/**").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/built/bundle.js").permitAll()
                .antMatchers("/style/**").permitAll()
                .antMatchers("/fonts/**").permitAll()
                .antMatchers("/node_modules/**").permitAll()

                .antMatchers("/api/users/profile").permitAll()
                .antMatchers("/api/users/getAll").permitAll()
                .antMatchers("/api/milestone/getAll").permitAll()
                .antMatchers("/api/awards/saveAward").permitAll()
                .antMatchers("/api/awards/getAllPendingApprovals").permitAll()

                .antMatchers("/handler/**").permitAll()
                .antMatchers("/achievement").permitAll()
                .antMatchers("/request").permitAll()
                .antMatchers("/").permitAll()

                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login").permitAll()
                .and().logout().deleteCookies("JSESSIONID").permitAll()
                .and().csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}


