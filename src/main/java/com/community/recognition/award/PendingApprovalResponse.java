package com.community.recognition.award;

import com.community.recognition.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PendingApprovalResponse {
  private Long id;
  private String awardeeName;
  private String awardeeTeam;
  private String awardeeDepartment;
  private String requestorName;
  private String requestorTeam;
  private String requestorDepartment;
  private boolean isSupervisor;
  private int containerCount;
  private String requestDate;
  private String attributes;
  private String remarks;
  private String approver;

  public static PendingApprovalResponse fromEntities(AwardEntity entity, UserEntity awardee, UserEntity requestor) {
    return new PendingApprovalResponse(
        entity.getId(),
        awardee.getFullName(),
        entity.getAwardeeTeam(),
        entity.getAwardeeCluster(),
        requestor.getFullName(),
        entity.getRequestorTeam(),
        entity.getRequestorCluster(),
        entity.isSupervisor(),
        entity.getPoints(),
        entity.getRequestedDate(),
        entity.getAttributes(),
        entity.getRemarks(),
            null
    );
  }

  public static PendingApprovalResponse forNotification(AwardEntity entity, UserEntity awardee, UserEntity requestor, String approver) {
    return new PendingApprovalResponse(
            entity.getId(),
            awardee.getFullName(),
            entity.getAwardeeTeam(),
            entity.getAwardeeCluster(),
            requestor.getFullName(),
            entity.getRequestorTeam(),
            entity.getRequestorCluster(),
            entity.isSupervisor(),
            entity.getPoints(),
            entity.getRequestedDate(),
            entity.getAttributes(),
            entity.getRemarks(),
            approver
    );
  }
}
