package com.community.recognition.award;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AwardResponse {

  private String action;
  private String name;

}
