package com.community.recognition.award;

import com.community.recognition.utils.AwardStatus;
import com.community.recognition.milestone.MilestoneService;
import com.community.recognition.notification.NotificationMessage;
import com.community.recognition.notification.NotificationService;
import com.community.recognition.user.UserEntity;
import com.community.recognition.user.UserService;
import com.community.recognition.utils.MessageUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AwardService {
    private AwardRepository awardRepository;
    private UserService userService;
    private MilestoneService milestoneService;
    private NotificationService notificationService;

    public Iterable<AwardEntity> getAll(){
        return awardRepository.findAll();
    }

    public Iterable<PendingApprovalResponse> getAllByApprovalStatus(String approvalStatus) {
        List<PendingApprovalResponse> pendingApprovals = new ArrayList<>();
        List<AwardEntity> entities = new ArrayList<>();
        awardRepository.findAllByApprovalStatus(approvalStatus).forEach(entities::add);
        entities.forEach(awardEntity -> {
            UserEntity awardeeEntity = userService.getUserById(awardEntity.getAwardeeUserId()).get();
            UserEntity requestorEntity = userService.getUserById(awardEntity.getRequestorUserId()).get();
            pendingApprovals
                .add(PendingApprovalResponse.fromEntities(awardEntity, awardeeEntity, requestorEntity));
        });
        return pendingApprovals;
    }

    public Optional<AwardEntity> getAward(Long id) {
        return awardRepository.findById(id);
    }

    public AwardEntity saveAward(AwardEntity awardEntity){
        AwardEntity entity = awardRepository.save(awardEntity);
        if(AwardStatus.PENDING.equals(entity.getApprovalStatus())) {
            UserEntity userEntity = userService.getUserById(awardEntity.getAwardeeUserId()).orElse(null);
            MessageUtil messageUtil = new MessageUtil(userEntity.getFullName());
            notificationService.broadcastToAll(NotificationMessage.createMessage(awardEntity.getId(), messageUtil.getMessage(AwardStatus.PENDING), AwardStatus.PENDING, userEntity.getAvatar(), ""));
        }
        return entity;
    }

    public AwardResponse updateAward(AwardEntity awardEntity, Long id, int pointsToAdd, String approver) throws Exception {
        if(!awardRepository.existsById(id)){
            throw new Exception("Id: " + id + "to be updated not found.");
        }
        UserEntity userEntity = userService.getUserById(awardEntity.getAwardeeUserId()).orElse(null);
        UserEntity approverEntity = userService.getUserByUsername(approver);
        MessageUtil messageUtil = new MessageUtil(userEntity.getFullName());

        if(AwardStatus.APPROVED.equals(awardEntity.getApprovalStatus())) {
            saveAward(awardEntity);

            boolean milestoneReached = milestoneService.updateMilestone(awardEntity, pointsToAdd);

            notificationService.broadcastToAll(NotificationMessage.createMessage(awardEntity.getId(), messageUtil.getMessage(AwardStatus.APPROVED), AwardStatus.APPROVED, userEntity.getAvatar(), approverEntity.getFullName()));
            if(milestoneReached) {
                notificationService.broadcastToAll(NotificationMessage.createMessage(awardEntity.getId(), messageUtil.getMessage(AwardStatus.MILESTONE_ACHIEVED), AwardStatus.MILESTONE_ACHIEVED, userEntity.getAvatar(), approverEntity.getFullName()));
            }

            return new AwardResponse(AwardStatus.APPROVED, userEntity.getFullName());
        }
        else if(AwardStatus.REJECTED.equals(awardEntity.getApprovalStatus())){
            saveAward(awardEntity);
        }

        return new AwardResponse(AwardStatus.UPDATED, userEntity.getFullName());
    }

    public void deleteAward(Long id) throws Exception {
        if(!awardRepository.existsById(id)){
            throw new Exception("Id: " + id + "to be deleted not found.");
        }else{
            awardRepository.deleteById(id);
        }
    }

    public PendingApprovalResponse getApprovedAwardById(Long id, String approver) {
        Optional<AwardEntity> awardEntity = awardRepository.findByIdAndApprovalStatus(id, AwardStatus.APPROVED);
        if(awardEntity.isPresent()) {
            UserEntity awardeeEntity = userService.getUserById(awardEntity.get().getAwardeeUserId()).get();
            UserEntity requestorEntity = userService.getUserById(awardEntity.get().getRequestorUserId()).get();

            return PendingApprovalResponse.forNotification(awardEntity.get(), awardeeEntity, requestorEntity, approver);
        }

        return null;
    }
}
