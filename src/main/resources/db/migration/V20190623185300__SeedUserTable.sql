insert into user(FIRSTNAME, USERNAME, PASSWORD, ROLE, MIDDLENAME, LASTNAME, DOMAIN, EMAIL, TEAM, AVATAR, DEPARTMENT)
values
-- approvers
('DENNIS', 'daluzde', '$2a$11$zgTDkPB4RoaxnDkzXKROlOaygu2HyAjl9..G.dz856Io252aUZQMu', 'APPROVER', '', 'DALUZ', 'ISDC', 'dennis.daluz@oocl.com', '', '', 'ISD'),
('FARA', 'gabrifa', '$2a$11$fZw6s14OKhluEitoCFt3GeWUshzKTuHJIuUzqqQ2XReU8nlnPufCm', 'APPROVER', '', 'GABRIEL', 'SHP', 'fara.gabriel@oocl.com', '', '', 'ISD'),
('ADMIN', 'recogbeta', '$2a$11$vbBFadZo1tetBKUDAL9ey.Bkcehx4RZoCBQU.Ly/GOYCtOlAQh9pK', 'APPROVER', '', 'ISTRATOR', 'ISDC', 'dummy_noreply@oocl.com', '', '', 'ISD'),
-- users
('RECOGUSER', 'recoguserbeta', '$2a$11$gGsgNwW9.qA6dJrZW.5qGOPrjAOnVENdDAp8x2Caoo8qEcIYqmqLG', 'EMPLOYEE', '', 'BETA', 'ISDC', 'recoguser_noreply@oocl.com','SHP','','ISD'),
('PETER', 'barrepe', '$2a$11$gdMbSMmaAu.bQABO.rbrW.IppGeIsA9KHxGZUCVZTcaVSDAxljT3K', 'EMPLOYEE', '', 'BARREDO', 'SHP', 'peter.emmanuel.barredo@oocl.com', 'N2C3', '', 'ISD'),
('PAOLO', 'florejo', '$2a$11$CinXH5nfDHxphwYm71BOP.Va8q.TkHIVeRkM8HGpt6pAOTnlsRjly', 'EMPLOYEE', '', 'FLORES', 'SHP', 'john.paolo.flores@oocl.com', 'N2C3', '', 'ISD'),
('GENE', 'kadange', '$2a$11$36viF7HqXQXWQwwUE6a7.OF9jph1VSk0aJmFz06iSx8jcNW5Ha7pG', 'EMPLOYEE', '', 'KADANO', 'SHP', 'gene.anthony.kadano@oocl.com', 'N2C3', '', 'ISD'),
('CARLO', 'licupca', '$2a$11$7RoYQWEoQ18GGUd6kUo37.VtFJExf1dscef7yF4asiAsxaBATOgzm', 'EMPLOYEE', '', 'LICUP', 'SHP', 'carlo.licup@oocl.com', 'FWDMAPPER', '', 'ISD'),
('CEDRIC DON', 'colomce', '$2a$11$H/.Ra25n8CPMTh2ofqn82.TjrngErpEv42EZ7f3rGx/p12b61Y7KK', 'EMPLOYEE', '', 'COLOMA', 'SHP', 'cedric.don.coloma@oocl.com', 'LODS', '', 'ISD'),
('JAMES MERVIN', 'tabucja', '$2a$11$dnlx68MlemztXHeKafJTYuo9TisoapmcnvnDdQffSG8GfBJY5Q/zS', 'EMPLOYEE', '', 'TABUCOL', 'SHP', 'james.mervin.tabucol@oocl.com', 'SHP-BKG', '', 'ISD'),
('TRACY CARMEL', 'resurtr', '$2a$11$werCwd3ml7xMHpXrYEdf7uCsL9ZYuSIDziUx7sFOpwGjqTT8u5njS', 'EMPLOYEE', '', 'RESURRECCION', 'GEDI', 'tracy.carmel.resurreccion@oocl.com', 'EDI', '', 'ISD'),
('ZIEGFRIED', 'flamezi', '$2a$11$7gGJjjlFD4u0JDm2gGiJ7.QXCJl0NpMaDkZNofqVdA4a5EIz5VVHq', 'EMPLOYEE', '', 'FLAMENO', 'SHP', 'ziegfried.morrisey.flameno@oocl.com', 'GSP-ITS', '', 'ISD'),
('LESTER JAN KIM', 'artiele', '$2a$11$jW5ik7pK817Ud3lRrAsWLOKg63WkiO19KRxQcR39JxiFIOpuBEB8K', 'EMPLOYEE', '', 'ARTIENDA', 'PLAN', 'lester.jan.kim.artienda@oocl.com', '', '', 'ISD');