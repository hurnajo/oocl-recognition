ALTER TABLE user
  ADD FIRSTNAME varchar(50) AFTER ID,
  ADD MIDDLENAME varchar(50),
  ADD LASTNAME varchar(50),
  ADD DOMAIN varchar(10),
  ADD EMAIL varchar(100),
  ADD TEAM varchar(50),
  ADD AVATAR varchar(50),
  ADD DEPARTMENT varchar(50);


