insert into award(AWARDEE_USER_ID, AWARDEE_TEAM, AWARDEE_DEPARTMENT, REQUESTOR_USER_ID, REQUESTOR_TEAM,
                  REQUESTOR_DEPARTMENT, IS_SUPERVISOR, ATTRIBUTES, REMARKS, APPROVAL_STATUS, APPROVAL_DATE, POINTS)

values(12, 'GSP-ITS', 'ISD', 7, 'N2C3', 'ISD', FALSE, 'positive, ropv, honesty', 'sample remarks', 'APPROVED', '20190102', 1),
      (11, 'EDI', 'ISD', 6, 'N2C3', 'ISD', FALSE, 'extraEffort, timeliness, honesty', 'sample remarks2', 'APPROVED', '20190202', 1),
      (9, 'LODS', 'ISD', 5, 'N2C3', 'ISD', FALSE, 'sied, ropv, skillApplication', 'sample remarks3', 'PENDING', '20190302', 1),
      (8, 'FWDMAPPER', 'ISD', 2, '', 'ISD', TRUE, 'positive, ropv, honesty, SAMPLE OTHER ATTRIBUTE', 'sample remarks BUT longer', 'PENDING', '20190404', 1),
      (6, 'N2C3', 'ISD', 5, 'N2C3', 'ISD', TRUE, 'OTHER ATTRIBUTE1, OTHER ATTRIBUTE2', 'sample remarks555', 'PENDING', '20190322', 1);