package com.community.recognition.integration;

import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Before;
import org.junit.Test;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class PendingApprovalLayerTest extends BaseIntegrationTest  {

  @Before
  public void setup(){

  }

  @Test
  public void testPendingApprovalLayer() throws InterruptedException {
    login("daluzde", "Password1");
    goToPage("Approval");

    FluentWebElement page = $(".stretched.fourteen.wide.column").first();
    FluentWebElement approvalDetails = page.$(".two.column.row").last();
    FluentList<FluentWebElement> pendingApprovals = approvalDetails.$(".column .content table thead~tbody tr");
    assertThat(pendingApprovals.size()).isEqualTo(4);

    //Approve first record
    pendingApprovals.first().$("#approve-btn").first().click();
    waitUntil(".ui.tiny.modal.transition.visible.active");
    FluentWebElement modal = $(".ui.tiny.modal.transition.visible.active").first();
    assertThat(modal.$(".header").first().textContent()).isEqualTo("Approve Award");
    assertThat(modal.$(".content").first().textContent()).isEqualTo("Are you sure you want to approve?");
    assertThat(modal.$(".actions .ui.buttons .ui.positive.vertical.animated.button").first().textContent()).isEqualTo("1 PointOkay");

    clickOn(modal.$(".actions .ui.buttons .ui.positive.vertical.animated.button").first());

    approvalDetails = page.$(".two.column.row").last();
    Thread.sleep(1000);
    pendingApprovals = approvalDetails.$(".column .content table thead~tbody tr");
    assertThat(pendingApprovals.size()).isEqualTo(3);
  }
}
