import {shallow} from 'enzyme'
import React from "react"
import {PendingApprovalPage} from "../../../main/js/pages/PendingApprovalPage"
import Table from "../../../main/js/components/Table"
import {findManyComponents} from "../testHelpers";
import RequestInfoCard from "../../../main/js/components/RequestInfoCard";


describe('Pending Approval Page ', () => {
    let props, wrapper

    beforeEach(() => {
        props = {
            actions:{
                closeRejectModal: jest.fn()
            },
            pendingApprovals : [{
                id: 5,
                awardeeName: "PAOLO FLORES",
                awardeeTeam: "N2C3",
                awardeeDepartment: "ISD",
                requestorName: "PETER BARREDO",
                requestorTeam: "N2C3",
                requestorDepartment: "ISD",
                containerCount: 1,
                requestDate: "20190322",
                attributes: "OTHER ATTRIBUTE1, OTHER ATTRIBUTE2",
                remarks: "sample remarks555",
                supervisor: true
            }]
        }
        wrapper = shallow(<PendingApprovalPage {...props}/>)
    })

    it('renders the pending requests table component and blank request info card', () => {
        expect(wrapper).toHaveComponent(Table)

        let filterInput = findManyComponents(wrapper,"Input")
        expect(filterInput.length).toEqual(1)

        let tableComponent = findManyComponents(wrapper, Table)
        const columns = tableComponent.props().columns
        expect(columns.length).toEqual(3)
        expect(columns[0].label).toEqual("Awardee")
        expect(columns[1].label).toEqual("Remarks")
        expect(columns[2].label).toEqual("Action")

        const data = tableComponent.props().datas[0]

        expect(data).toEqual({
                id: 5,
                awardeeName: "PAOLO FLORES",
                awardeeTeam: "N2C3",
                awardeeDepartment: "ISD",
                requestorName: "PETER BARREDO",
                requestorTeam: "N2C3",
                requestorDepartment: "ISD",
                containerCount: 1,
                requestDate: "20190322",
                attributes: "OTHER ATTRIBUTE1, OTHER ATTRIBUTE2",
                remarks: "sample remarks555",
                supervisor: true
            })

        const requestInfoCard = findManyComponents(wrapper, RequestInfoCard)
        expect(requestInfoCard.length).toEqual(1)
        expect(requestInfoCard.data).toBeUndefined()
    })

    it('filters out items by awardeeName', () => {
        props.pendingApprovals = [...props.pendingApprovals,
            {
                id: 6,
                awardeeName: "JOBERT SUCALDITO",
                awardeeTeam: "BUZZ",
                awardeeDepartment: "ISD",
                requestorName: "PETER BARREDO",
                requestorTeam: "N2C3",
                requestorDepartment: "ISD",
                containerCount: 1,
                requestDate: "20190323",
                attributes: "Special Attribute",
                remarks: "sample remark",
                supervisor: false
            }]

        wrapper = shallow(<PendingApprovalPage {...props}/>)

        let tableComponent = findManyComponents(wrapper, Table)
        let datas = tableComponent.props().datas

        expect(datas.length).toEqual(2)

        let event = {
            target: {
                name: 'filterInput',
                value: 'PAO'
            }
        }
        let filterInput = findManyComponents(wrapper, "Input")
        filterInput.simulate("change", event);
        wrapper.update()

        datas = findManyComponents(wrapper, Table).props().datas
        expect(datas.length).toEqual(1)
        expect(datas[0]).toEqual({
            id: 5,
            awardeeName: "PAOLO FLORES",
            awardeeTeam: "N2C3",
            awardeeDepartment: "ISD",
            requestorName: "PETER BARREDO",
            requestorTeam: "N2C3",
            requestorDepartment: "ISD",
            containerCount: 1,
            requestDate: "20190322",
            attributes: "OTHER ATTRIBUTE1, OTHER ATTRIBUTE2",
            remarks: "sample remarks555",
            supervisor: true
        })
    })


})