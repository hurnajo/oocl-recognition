import attributes from "../../../main/js/util/attributes"

describe('Constant attributes testing', () => {

    it('should have a fix attribute value ', () => {
        //scenario to catch editing constant attribute value  - we do not edit attribute value coz it is saved
        //in DB . Recommend to just add another attribute in the constant if no other way out
        let constantAttributeValues = ['positive', 'followUp', 'creativity', 'ropv', 'honesty', 'dedication'
            , 'thoroughness', 'edo', 'sharing','accuracy','skillApplication','sied','flexible','timeliness','extraEffort'
            , 'ragc', 'caring', 'sensitivity', 'cda', 'cal' , 'quality', 'responsiveness', 'isATeamMember']

        attributes.map((attribute) => {
            expect(attribute.code).toBeIn(constantAttributeValues)
        })
    })
})