import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/maintenance/actions'
import * as types from '../../../../main/js/actionTypes'
import axios from "axios"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('Maintenance actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({})
    })

    it('handles fetch all users', () => {
        const expectedActions = [
            {type: types.FETCH_USER}
        ]
        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
        }]
        const expectedURL = '/api/user'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve(mockReturnValue))

        return store.dispatch(actions.fetchAllUser()).then(()=>{
            expect(axios.get).toHaveBeenCalledWith(expectedURL)
            expect(store.getActions()).toEqual(expectedActions)
            axios.get.mockReset()
        })

    })
})