import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/pendingApproval/actions'
import * as types from '../../../../main/js/actionTypes'
import axios from "axios"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('Pending Approval actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({})
    })

    it('handles fetchAllPendingApproval', () => {
        const expectedActions = [
            {type: types.FETCH_PENDING_APPROVAL}
        ]

        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
        }]
        const expectedURL = '/api/awards/getAllPendingApprovals'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve(mockReturnValue))

        return store.dispatch(actions.fetchAllPendingApproval()).then(() =>{
            expect(axios.get).toHaveBeenCalledWith(expectedURL)
            expect(store.getActions()).toEqual(expectedActions)
            axios.get.mockReset()
        })

    })

    //nagdadalawang isip ako kung idadaan ko sa redux yung pagclose o hindi --florejo
    // it('handles openRejectModal', () => {
    //     const expectedActions = [
    //         {type: types.OPEN_REJECT_MODAL}
    //     ]
    //
    //     store.dispatch(actions.openRejectModal())
    //     expect(store.getActions()).toEqual(expectedActions)
    // })
    //
    // it('handles closeRejectModal', () => {
    //     const expectedActions = [
    //         {type: types.CLOSE_REJECT_MODAL}
    //     ]
    //
    //     store.dispatch(actions.closeRejectModal())
    //     expect(store.getActions()).toEqual(expectedActions)
    // })

    it('handles submitApproval without points', () => {
        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
            approvalStatus: "PENDING"
        }]
        const expectedGetAwardURL = '/api/awards/getAward/1'
        const expectedUpdateAwardURL = '/api/awards/updateAward/1/daluzde'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve({data: mockReturnValue}))
        jest.spyOn(axios, "post").mockReturnValueOnce(Promise.resolve({data: {action: "APPROVED", name: "John Doe"}}))

        return store.dispatch(actions.submitApproval({id: 1}, null, 'daluzde')).then(() =>{
            expect(axios.get).toHaveBeenCalledWith(expectedGetAwardURL)
            expect(axios.post).toHaveBeenCalledWith(expectedUpdateAwardURL, mockReturnValue)
            axios.get.mockReset()
            axios.post.mockReset()
        })
    })

    it('handles submitApproval with points', () => {
        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
            approvalStatus: "PENDING"
        }]
        const expectedGetAwardURL = '/api/awards/getAward/1'
        const expectedUpdateAwardURL = '/api/awards/updateAward/1/1/daluzde'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve({data: mockReturnValue}))
        jest.spyOn(axios, "post").mockReturnValueOnce(Promise.resolve({data: {action: "APPROVED", name: "John Doe"}}))

        return store.dispatch(actions.submitApproval({id: 1}, 1, 'daluzde')).then(() =>{
            expect(axios.get).toHaveBeenCalledWith(expectedGetAwardURL)
            expect(axios.post).toHaveBeenCalledWith(expectedUpdateAwardURL, mockReturnValue)
            axios.get.mockReset()
            axios.post.mockReset()
        })
    })

    it('handles fetch approved award', () => {
        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
            approvalStatus: "APPROVED"
        }]
        const expectedActions = [
            {type: types.FETCH_APPROVED_AWARD_REQUEST},
            {type: types.FETCH_APPROVED_AWARD_SUCCESS, payload: mockReturnValue}
        ]
        const expectedURL = '/api/awards/getApprovedAward/1/daluzde'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve({data: mockReturnValue}))

        return store.dispatch(actions.fetchApprovedAward( 1,'daluzde')).then(() =>{
            expect(axios.get).toHaveBeenCalledWith(expectedURL)
            expect(store.getActions()).toEqual(expectedActions)
            axios.get.mockReset()
        })
    })
})