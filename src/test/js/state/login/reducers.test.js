import reducer from "../../../../main/js/state/login/reducers"
import * as types from "../../../../main/js/actionTypes"
import {checkReducerFn, clone} from "../../testHelpers"

describe('login reducers', () => {
    let checkReducer

    const initialState = {
        userProfile: {}
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('handles save user', () => {
        const action = {type: types.SAVE_USER, payload : {userName : 'Almost', motto : 'Dalagang Pilipina Yeah'}}
        const expectedState = clone(initialState)
        expectedState.userProfile = {userName : 'Almost', motto : 'Dalagang Pilipina Yeah'}
        checkReducer(action, expectedState)
    })

})