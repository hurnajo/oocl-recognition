import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/login/actions'
import * as types from '../../../../main/js/actionTypes'
import axios from "axios"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('Login actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({})
    })

    it('handles login', () => {
        const expectedActions = [
            {type: types.SAVE_USER, payload : 'success'}
        ]
        const mockSucessLogin = {data : 'success'}
        const expectedURL = '/api/login'
        jest.spyOn(axios, "post").mockReturnValue(Promise.resolve( mockSucessLogin ))

        return store.dispatch(actions.login('john','cena')).then(()=>{
            expect(axios.post).toHaveBeenCalledWith(expectedURL , {username : 'john', password: 'cena'})
            expect(store.getActions()).toEqual(expectedActions)
            axios.post.mockReset()
        })

    })
})