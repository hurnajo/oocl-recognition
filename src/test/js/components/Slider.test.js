import {shallow} from 'enzyme'
import React from "react"

import Slider from "../../../main/js/components/Slider"

describe('Slider', () => {
    it('renders the component', () => {
        const wrapper = shallow(<Slider/>)
        expect(wrapper.text()).toEqual('<>')
    })

})