import {shallow} from 'enzyme'
import React from "react"

import CheckboxAttributes from "../../../main/js/components/CheckboxAttributes"
import {Checkbox} from "semantic-ui-react"

describe('Check Box attributes ', () => {

    it('renders the component', () => {
        let props = {
            checked : false,
            width : 10,
            name : 'checkbox',
            label : 'checkbox',
            checkboxChanged : jest.fn()
        }
        const wrapper = shallow(<CheckboxAttributes {...props}/>)
        expect(wrapper).toHaveComponent(Checkbox)
    })

})