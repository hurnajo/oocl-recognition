import Enzyme from "enzyme/build/index";
import Adapter from "enzyme-adapter-react-16/build/index";

global.fetch = require('jest-fetch-mock');

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
})

Enzyme.configure({
    adapter: new Adapter(),
    disableLifecycleMethods: true,
})