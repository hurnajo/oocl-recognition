package com.community.recognition.milestone;

import com.community.recognition.user.UserEntity;
import java.util.Date;

public class MilestoneEntityBuilder {

  MilestoneEntity entity;

  public MilestoneEntityBuilder() {
    this.entity = new MilestoneEntity();
  }

  public MilestoneEntityBuilder setId(Long id) {
    this.entity.setId(id);
    return this;
  }

  public MilestoneEntityBuilder setCreatedDate(Date createdDate) {
    this.entity.setCreated_date(createdDate);
    return this;
  }

  public MilestoneEntityBuilder setVoyage(Long voyage) {
    this.entity.setVoyage(voyage);
    return this;
  }

  public MilestoneEntityBuilder setPoints(Long points) {
    this.entity.setPoints(points);
    return this;
  }

  public MilestoneEntityBuilder setUser(UserEntity user) {
    this.entity.setUser(user);
    return this;
  }

  public MilestoneEntity build() {
    return this.entity;
  }

  public MilestoneEntity initialData(UserEntity userEntity) {
    setId(1L);
    setCreatedDate(new Date(2019, 07, 07));
    setVoyage(1L);
    setPoints(2L);
    setUser(userEntity);

    return this.entity;
  }

}
