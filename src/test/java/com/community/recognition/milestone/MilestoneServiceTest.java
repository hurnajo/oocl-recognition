package com.community.recognition.milestone;

import com.community.recognition.award.AwardEntity;
import com.community.recognition.award.AwardEntityBuilder;
import com.community.recognition.user.UserEntity;
import com.community.recognition.user.UserEntityBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MilestoneServiceTest {
    
    private MilestoneService milestoneService;
    
    @Mock
    private MilestoneRepository milestoneRepository;

    @Before
    public void setUp() {
        milestoneService = new MilestoneService(milestoneRepository);
    }

    @Test
    public void getAll() {
        UserEntity userEntity = new UserEntityBuilder().initialData();
        MilestoneEntity milestoneEntity = new MilestoneEntityBuilder().initialData(userEntity);
        when(milestoneRepository.findAll()).thenReturn(Collections.singletonList(milestoneEntity));

        assertThat(milestoneService.getAll()).isEqualTo(Collections.singletonList(milestoneEntity));
    }

    @Test
    public void findUser() {
        UserEntity userEntity = new UserEntityBuilder().initialData();
        MilestoneEntity milestoneEntity = new MilestoneEntityBuilder().initialData(userEntity);
        when(milestoneRepository.findByUserId(1L)).thenReturn(milestoneEntity);

        assertThat(milestoneService.findMilestoneByUserId(1L)).isEqualTo(milestoneEntity);
    }

    @Test
    public void save() {
        UserEntity userEntity = new UserEntityBuilder().initialData();
        MilestoneEntity milestoneEntity = new MilestoneEntityBuilder().initialData(userEntity);
        when(milestoneRepository.save(milestoneEntity)).thenReturn(milestoneEntity);

        assertThat(milestoneService.save(milestoneEntity)).isEqualTo(milestoneEntity);
    }

    @Test
    public void updateMilestoneWhereMilestoneNotReached() {
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();
        UserEntity userEntity = new UserEntityBuilder().initialData();
        MilestoneEntity milestoneEntity = new MilestoneEntityBuilder().initialData(userEntity);
        when(milestoneRepository.findByUserId(1L)).thenReturn(milestoneEntity);
        when(milestoneRepository.save(milestoneEntity)).thenReturn(milestoneEntity);

        assertThat(milestoneService.updateMilestone(awardEntity, 1)).isEqualTo(false);
    }

    @Test
    public void updateMilestoneWhereMilestoneReached() {
        AwardEntity awardEntity = new AwardEntityBuilder().initialData();
        UserEntity userEntity = new UserEntityBuilder().initialData();
        MilestoneEntity milestoneEntity = new MilestoneEntityBuilder().initialData(userEntity);
        milestoneEntity.setVoyage(1L);
        milestoneEntity.setPoints(11L);
        when(milestoneRepository.findByUserId(1L)).thenReturn(milestoneEntity);
        when(milestoneRepository.save(milestoneEntity)).thenReturn(milestoneEntity);

        assertThat(milestoneService.updateMilestone(awardEntity, 1)).isEqualTo(true);
    }
}