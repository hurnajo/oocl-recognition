package com.community.recognition.award;

public class PendingApprovalResponseBuilder {

  private PendingApprovalResponse response;

  public PendingApprovalResponseBuilder() {
    this.response = new PendingApprovalResponse();
  }

  public PendingApprovalResponseBuilder setid(Long id) {
    this.response.setId(id);
    return this;
  }

  public PendingApprovalResponseBuilder setAwardeeName(String awardeeName) {
    this.response.setAwardeeName(awardeeName);
    return this;
  }

  public PendingApprovalResponseBuilder setAwardeeTeam(String awardeeTeam) {
    this.response.setAwardeeTeam(awardeeTeam);
    return this;
  }

  public PendingApprovalResponseBuilder setAwardeeDepartment(String awardeeDepartment) {
    this.response.setAwardeeDepartment(awardeeDepartment);
    return this;
  }

  public PendingApprovalResponseBuilder setRequestorName(String requestorName) {
    this.response.setRequestorName(requestorName);
    return this;
  }

  public PendingApprovalResponseBuilder setRequestorTeam(String requestorTeam) {
    this.response.setRequestorTeam(requestorTeam);
    return this;
  }

  public PendingApprovalResponseBuilder setRequestorDepartment(String requestorDepartment) {
    this.response.setRequestorDepartment(requestorDepartment);
    return this;
  }

  public PendingApprovalResponseBuilder setSupervisor(boolean isSupervisor) {
    this.response.setSupervisor(isSupervisor);
    return this;
  }

  public PendingApprovalResponseBuilder setContainerCount(int containerCount) {
    this.response.setContainerCount(containerCount);
    return this;
  }

  public PendingApprovalResponseBuilder setRequestDate(String requestDate) {
    this.response.setRequestDate(requestDate);
    return this;
  }

  public PendingApprovalResponseBuilder setAttributes(String attributes) {
    this.response.setAttributes(attributes);
    return this;
  }

  public PendingApprovalResponseBuilder setRemarks(String remarks) {
    this.response.setRemarks(remarks);
    return this;
  }

  public PendingApprovalResponse build() {
    return this.response;
  }

  public PendingApprovalResponse initialData() {
    setid(1L);
    setAwardeeName("John Doe");
    setAwardeeTeam("SHP");
    setAwardeeDepartment("SHIPMENT SERVICES");
    setRequestorName("John Cena");
    setRequestorTeam("SHP");
    setRequestorDepartment("SHIPMENT SERVICES");
    setSupervisor(false);
    setContainerCount(1);
    setRequestDate("07/07/2019");
    setAttributes("Sample Attributes");
    setRemarks("Sample Remarks");
    return this.response;
  }

}
